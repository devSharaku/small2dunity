// ========================================================================
// Copyright (c) 2021 DreamStorm Studios Piotr Penar. All Rights Reserved.
// Confidential and Proprietary - Protected under copyright and other laws.
// ========================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ScriptTemplates.Editor
{
    internal sealed class ScriptKeywordProcessor : UnityEditor.AssetModificationProcessor
    {
        #region Private Fields

        private static char[] spliters = { '/', '\\', '.' };
        private static List<string> wordsToDelete = new List<string> { "Scripts" };

        #endregion

        #region Public Methods

        public static void OnWillCreateAsset(string path)
        {
            path = path.Replace(".meta", "");

            int index = path.LastIndexOf(".", StringComparison.Ordinal);

            if (index < 0)
            {
                return;
            }

            string file = path.Substring(index);

            if (file != ".cs")
            {
                return;
            }

            List<string> namespaces = path.Split(spliters).ToList();
            namespaces = namespaces.GetRange(1, namespaces.Count - 3);
            namespaces = namespaces.Except(wordsToDelete).ToList();

            string namespaceString = namespaces.Count == 0 ? "Globals" : string.Join(".", namespaces);
            string systemString;

            if (namespaces.Count == 0)
            {
                systemString = "nameof(Globals)";
            }
            else if (namespaces.Count == 1)
            {
                systemString = $"nameof({namespaces[0]})";
            }
            else
            {
                systemString = $"nameof({namespaces[0]}) + \"/\" + nameof({namespaces[1]})";
            }

            index = Application.dataPath.LastIndexOf("Assets", StringComparison.Ordinal);
            path = Application.dataPath.Substring(0, index) + path;

            if (!File.Exists(path))
            {
                return;
            }

            string fileContent = File.ReadAllText(path);
            fileContent = fileContent.Replace("#NAMESPACE#", namespaceString);
            fileContent = fileContent.Replace("#SYSTEMNAME#", systemString);
            File.WriteAllText(path, fileContent);
            AssetDatabase.Refresh();
        }

        #endregion
    }
}