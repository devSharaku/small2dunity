namespace UI.Interfaces
{
    public interface IInitializable
    {
        #region Public Methods

        public void Initialize();
        public void Deinitialize();

        #endregion
    }
}