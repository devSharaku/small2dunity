using Events;
using UI.Enums;
using UnityEngine;
using Utils;

namespace UI.PanelControllers
{
    [CreateAssetMenu(fileName = nameof(MainMenuPanelController), menuName = "ScriptableObjects/" + nameof(UI) + "/" + nameof(PanelControllers) + "/" + nameof(MainMenuPanelController))]
    public class MainMenuPanelController : ABasePanelController
    {
        #region Serialized Fields

        [SerializeField]
        private LoadScene loadScene;

        #endregion

        #region Public Methods

        public void PlayGame()
        {
            loadScene.Invoke(Constants.GAMEPLAY_SCENE_NAME);
        }

        public void OpenSettings()
        {
            openPanel.Invoke(PanelType.Settings);
        }

        public void ExitGame()
        {
            Application.Quit();
        }

        #endregion
    }
}