using System;
using UI.Enums;
using UI.Events;
using UI.Interfaces;
using UnityEngine;

namespace UI.PanelControllers
{
    public abstract class ABasePanelController : ScriptableObject, IInitializable
    {
        #region Events

        public event Action<bool> OnTogglePanel;

        #endregion

        #region Serialized Fields

        [SerializeField]
        private PanelType panelType;
        [SerializeField]
        protected OpenPanel openPanel;

        #endregion

        #region Private Fields

        private bool isOpen;

        #endregion

        #region Public Methods

        public void Initialize()
        {
            AssignCallbacks();
            isOpen = true;
        }

        public void Deinitialize()
        {
            UnassignCallbacks();
        }

        #endregion

        #region Protected Methods

        protected virtual void AssignCallbacks()
        {
            openPanel.OnAction += TogglePanel;
        }

        protected virtual void UnassignCallbacks()
        {
            openPanel.OnAction -= TogglePanel;
        }

        protected virtual void OnPanelOpen()
        {
            isOpen = true;
        }

        protected virtual void OnPanelClosed()
        {
            isOpen = false;
        }

        #endregion

        #region Private Methods

        private void TogglePanel(PanelType panelToOpen)
        {
            if (panelToOpen == panelType)
            {
                OnTogglePanel?.Invoke(true);
                OnPanelOpen();
            }
            else if (isOpen)
            {
                OnTogglePanel?.Invoke(false);
                OnPanelClosed();
            }
        }

        #endregion
    }
}