using UI.Enums;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UI.PanelControllers
{
    [CreateAssetMenu(fileName = nameof(SettingsPanelController), menuName = "ScriptableObjects/" + nameof(UI) + "/" + nameof(PanelControllers) + "/" + nameof(SettingsPanelController))]
    public class SettingsPanelController : ABasePanelController
    {
        #region Serialized Fields

        [SerializeField]
        private InputActionReference inputActionReference;

        #endregion

        #region Public Methods

        public void BackToMainMenu()
        {
            openPanel.Invoke(PanelType.MainMenu);
        }

        #endregion

        #region Protected Methods

        protected override void OnPanelClosed()
        {
            base.OnPanelClosed();
            inputActionReference.action.performed -= BackButtonClicked;
        }

        protected override void OnPanelOpen()
        {
            base.OnPanelOpen();
            inputActionReference.action.performed += BackButtonClicked;
        }

        #endregion

        #region Private Methods

        private void BackButtonClicked(InputAction.CallbackContext obj)
        {
            BackToMainMenu();
        }

        #endregion
    }
}