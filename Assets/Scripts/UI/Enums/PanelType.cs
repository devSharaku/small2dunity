namespace UI.Enums
{
    public enum PanelType
    {
        None = 0,
        MainMenu = 1,
        Settings = 2
    }
}