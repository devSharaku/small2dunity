using UI.Interfaces;
using UI.PanelControllers;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Panels
{
    public abstract class ABasePanel<T> : MonoBehaviour, IInitializable where T : ABasePanelController
    {
        #region Serialized Fields

        [SerializeField]
        protected T panelController;
        [SerializeField]
        private GameObject selectOnOpen;

        #endregion

        #region Public Methods

        public void Initialize()
        {
            AssignCallbacks();
        }

        public void Deinitialize()
        {
            UnassignCallbacks();
        }

        #endregion

        #region Protected Methods

        protected virtual void AssignCallbacks()
        {
            panelController.OnTogglePanel += TogglePanel;
        }

        protected virtual void UnassignCallbacks()
        {
            panelController.OnTogglePanel -= TogglePanel;
        }

        #endregion

        #region Private Methods

        private void TogglePanel(bool state)
        {
            gameObject.SetActive(state);

            if (state && selectOnOpen != null)
            {
                EventSystem.current.SetSelectedGameObject(selectOnOpen);
            }
        }

        #endregion
    }
}