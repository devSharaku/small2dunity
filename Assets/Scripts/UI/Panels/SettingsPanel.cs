using UI.PanelControllers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Panels
{
    public class SettingsPanel : ABasePanel<SettingsPanelController>
    {
        #region Serialized Fields

        [SerializeField]
        private Button backButton;

        #endregion

        #region Protected Methods

        protected override void AssignCallbacks()
        {
            base.AssignCallbacks();
            backButton.onClick.AddListener(panelController.BackToMainMenu);
        }

        protected override void UnassignCallbacks()
        {
            base.UnassignCallbacks();
            backButton.onClick.RemoveListener(panelController.BackToMainMenu);
        }

        #endregion
    }
}