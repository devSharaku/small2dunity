using UI.PanelControllers;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Panels
{
    public class MainMenuPanel : ABasePanel<MainMenuPanelController>
    {
        #region Serialized Fields

        [SerializeField]
        private Button playButton;
        [SerializeField]
        private Button settingsButton;
        [SerializeField]
        private Button exitButton;

        #endregion

        #region Protected Methods

        protected override void AssignCallbacks()
        {
            base.AssignCallbacks();
            playButton.onClick.AddListener(panelController.PlayGame);
            settingsButton.onClick.AddListener(panelController.OpenSettings);
            exitButton.onClick.AddListener(panelController.ExitGame);
        }

        protected override void UnassignCallbacks()
        {
            base.UnassignCallbacks();
            playButton.onClick.RemoveListener(panelController.PlayGame);
            settingsButton.onClick.RemoveListener(panelController.OpenSettings);
            exitButton.onClick.RemoveListener(panelController.ExitGame);
        }

        #endregion
    }
}