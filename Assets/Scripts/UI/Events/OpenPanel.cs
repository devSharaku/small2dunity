using Events;
using UI.Enums;
using UnityEngine;

namespace UI.Events
{
    [CreateAssetMenu(fileName = nameof(OpenPanel), menuName = "ScriptableObjects/" + nameof(UI) + "/" + nameof(Events) + "/" + nameof(OpenPanel))]
    public class OpenPanel : GameEventWithParameter<PanelType>
    {
    }
}