using Gameplay.Data;
using UnityEngine;

namespace Gameplay
{
    public class CameraBounds : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private MovementData movementData;

        #endregion

        #region Private Fields

        private Camera camera;

        #endregion

        #region Unity Callbacks

        public void Awake()
        {
            GetCameraBounds();
        }

        #endregion

        #region Private Methods

        private void GetCameraBounds()
        {
            camera = GetComponent<Camera>();
            Vector2 leftBottom = camera.ScreenToWorldPoint(new Vector3(0, 0, camera.nearClipPlane));
            Vector2 rightTop = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth, camera.pixelHeight, camera.nearClipPlane));
            movementData.SetCameraBounds(leftBottom, rightTop);
        }

        #endregion
    }
}