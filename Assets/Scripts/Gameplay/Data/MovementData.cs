using UnityEngine;

namespace Gameplay.Data
{
    [CreateAssetMenu(fileName = nameof(MovementData), menuName = "ScriptableObjects/" + nameof(Gameplay) + "/" + nameof(MovementData))]
    public class MovementData : ScriptableObject
    {
        #region Public Properties

        public Vector2 LeftBottom { get; private set; }
        public Vector2 RightTop { get; private set; }
        [field: SerializeField]
        public float EdgesOffset { get; private set; }
        [field: SerializeField]
        public float PlayerMovementSpeed { get; private set; }
        [field: SerializeField]
        public float EnemyMovementSpeed { get; private set; }

        #endregion

        #region Public Methods

        public void SetCameraBounds(Vector2 leftBottom, Vector2 rightTop)
        {
            LeftBottom = leftBottom;
            RightTop = rightTop;
            LeftBottom = new Vector2(LeftBottom.x + EdgesOffset, LeftBottom.y + EdgesOffset);
            RightTop = new Vector2(RightTop.x - EdgesOffset, RightTop.y - EdgesOffset);
        }

        #endregion
    }
}