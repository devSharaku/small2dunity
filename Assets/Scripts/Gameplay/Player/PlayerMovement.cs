using Gameplay.Data;
using UnityEngine;
using Utils;

namespace Gameplay.Player
{
    public class PlayerMovement : MonoBehaviour, IInputListener
    {
        #region Serialized Fields

        [SerializeField]
        private MovementData movementData;

        #endregion

        #region Private Fields

        private Vector2 targetPosition;

        #endregion

        #region Public Methods

        public void MovementPerformed(Vector2 movementVector)
        {
            targetPosition = Vector2.MoveTowards(transform.position, (Vector2)transform.position + movementVector, movementData.PlayerMovementSpeed);
            ClampTargetPosition();
            transform.position = targetPosition;
        }

        public void MoveTo(Vector2 position)
        {
            //ToDo : Change it to some lerp  
            Vector2 direction = position - (Vector2)transform.position;
            MovementPerformed(direction);
        }

        #endregion

        #region Private Methods

        private void ClampTargetPosition()
        {
            targetPosition.x = Mathf.Clamp(targetPosition.x, movementData.LeftBottom.x, movementData.RightTop.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, movementData.LeftBottom.y, movementData.RightTop.y);
        }

        #endregion
    }
}