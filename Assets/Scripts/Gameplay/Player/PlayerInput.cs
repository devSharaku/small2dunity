using Events;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils;

namespace Gameplay.Player
{
    public class PlayerInput : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private InputActionReference movementActionReference;
        [SerializeField]
        private Camera camera;
        [SerializeField]
        private InputActionReference moveToActionReference;
        [SerializeField]
        private InputActionReference backToMenu;
        [SerializeField]
        private InputActionReference mousePositionActionReference;
        [SerializeField]
        private LoadScene loadScene;
        [SerializeField]
        private IInputListener inputListener;

        #endregion

        #region Private Fields

        private bool moveWithMouse;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            inputListener = GetComponent<IInputListener>();
            AssignCallbacks();
        }

        private void OnDestroy()
        {
            UnassignCallbacks();
        }

        private void Update()
        {
            if (moveWithMouse)
            {
                inputListener.MoveTo(camera.ScreenToWorldPoint(mousePositionActionReference.action.ReadValue<Vector2>()));

                return;
            }

            inputListener.MovementPerformed(movementActionReference.action.ReadValue<Vector2>());
        }

        #endregion

        #region Private Methods

        private void AssignCallbacks()
        {
            backToMenu.action.performed += BackToMenu;
            moveToActionReference.action.started += MouseButtonPressed;
            moveToActionReference.action.canceled += MouseButtonStopped;
        }

        private void MouseButtonStopped(InputAction.CallbackContext obj)
        {
            moveWithMouse = false;
        }

        private void MouseButtonPressed(InputAction.CallbackContext obj)
        {
            moveWithMouse = true;
        }

        private void UnassignCallbacks()
        {
            backToMenu.action.performed -= BackToMenu;
            moveToActionReference.action.started -= MouseButtonPressed;
            moveToActionReference.action.canceled -= MouseButtonStopped;
        }

        //ToDo : Move it to game manager or something like that
        private void BackToMenu(InputAction.CallbackContext callbackContext)
        {
            loadScene.Invoke(Constants.MAIN_MENU_SCENE_NAME);
        }

        #endregion
    }
}