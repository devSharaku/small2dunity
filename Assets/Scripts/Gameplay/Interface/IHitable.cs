namespace Gameplay.Interface
{
    public interface IHitable
    {
        #region Public Methods

        public void GotHit();

        #endregion
    }
}