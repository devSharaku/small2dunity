using Gameplay.Data;
using UnityEngine;

namespace Gameplay.Enemy
{
    public class EnemySpawner : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private MovementData movementData;
        [SerializeField]
        private int enemyNumber;
        [SerializeField]
        private EnemyMovement enemyPrefab;
        [SerializeField]
        private Transform enemiesParent;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            for (int enemyCounter = 0; enemyCounter < enemyNumber; enemyCounter++)
            {
                Vector2 positionToSpawn = new Vector2(Random.Range(movementData.LeftBottom.x, movementData.RightTop.x), Random.Range(movementData.LeftBottom.y, movementData.RightTop.y));
                Instantiate(enemyPrefab, positionToSpawn, Quaternion.identity, enemiesParent);
            }
        }

        #endregion
    }
}