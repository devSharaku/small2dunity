using Gameplay.Interface;
using UnityEngine;

namespace Gameplay.Enemy
{
    public class EnemyCollision : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private SpriteRenderer spriteRenderer;

        #endregion

        #region Private Fields

        private IHitable hitable;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            hitable = GetComponentInParent<IHitable>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                hitable.GotHit();
                spriteRenderer.color = Color.blue;
                Destroy(gameObject);
            }
        }

        #endregion
    }
}