using Gameplay.Data;
using Gameplay.Interface;
using UnityEngine;

namespace Gameplay.Enemy
{
    public class EnemyMovement : MonoBehaviour, IHitable
    {
        #region Serialized Fields

        [SerializeField]
        private MovementData movementData;

        #endregion

        #region Private Fields

        private Vector2 direction;
        private Vector2 targetPosition;
        private bool shouldMove;

        #endregion

        #region Unity Callbacks

        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                direction = -(other.transform.position - transform.position).normalized;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            direction = Vector2.zero;
        }

        private void Update()
        {
            targetPosition = Vector2.MoveTowards(transform.position, (Vector2)transform.position + direction, movementData.EnemyMovementSpeed);
            targetPosition.x = Mathf.Clamp(targetPosition.x, movementData.LeftBottom.x, movementData.RightTop.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, movementData.LeftBottom.y, movementData.RightTop.y);
            transform.position = targetPosition;
        }

        #endregion

        #region Public Methods

        public void GotHit()
        {
            Destroy(this);
        }

        #endregion
    }
}