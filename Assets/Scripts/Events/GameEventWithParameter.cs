using System;
using UnityEngine;

namespace Events
{
    public abstract class GameEventWithParameter<T> : ScriptableObject
    {
        #region Events

        public event Action<T> OnAction;

        #endregion

        #region Public Methods

        public virtual void Invoke(T parameter)
        {
            OnAction?.Invoke(parameter);
        }

        #endregion
    }
}