using UnityEngine;

namespace Events
{
    [CreateAssetMenu(fileName = nameof(LoadScene), menuName = "ScriptableObjects/" + nameof(Events) + "/" + nameof(LoadScene))]
    public class LoadScene : GameEventWithParameter<string>
    {
    }
}