using System.Collections.Generic;
using UI;
using UI.Interfaces;
using UnityEngine;

namespace Utils
{
    public class Initializator : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private List<Object> scriptableObjectsToInitialize;

        #endregion

        #region Unity Callbacks

        private void Awake()
        {
            HandleInitialization(true);
        }

        private void OnDestroy()
        {
            HandleInitialization(false);
        }

        #endregion

        #region Private Methods

        private void HandleInitialization(bool initialize)
        {
            foreach (Object currentObject in scriptableObjectsToInitialize)
            {
                if (currentObject is IInitializable initializable)
                {
                    if (initialize)
                    {
                        initializable.Initialize();
                    }
                    else
                    {
                        initializable.Deinitialize();
                    }
                }
                else
                {
                    Debug.LogError($"{currentObject.name} is not implementing interface IInitializable");
                }
            }
        }

        #endregion
    }
}