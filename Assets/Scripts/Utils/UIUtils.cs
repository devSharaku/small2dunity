using UnityEditor;
using UnityEngine;

namespace Utils
{
    public class UIUtilities : MonoBehaviour
    {
#if UNITY_EDITOR

        #region Public Methods

        [MenuItem("UI Utilities/Corners to Anchors %]")]
        public static void CornersToAnchors()
        {
            Transform[] selectedTransforms = Selection.transforms;
            Undo.RegisterCompleteObjectUndo(selectedTransforms, "Corners To Anchors");

            foreach (Transform transform in selectedTransforms)
            {
                RectTransform t = transform as RectTransform;

                if (t == null)
                {
                    return;
                }

                t.offsetMin = t.offsetMax = new Vector2(0, 0);
            }
        }

        #endregion

        [MenuItem("UI Utilities/Anchors to Corners %[")]
        public static void AnchorsToCorners()
        {
            Transform[] selectedTransforms = Selection.transforms;
            Undo.RegisterCompleteObjectUndo(selectedTransforms, "Anchors to Corners");

            foreach (Transform transform in selectedTransforms)
            {
                RectTransform t = transform as RectTransform;
                RectTransform pt = Selection.activeTransform.parent as RectTransform;

                if (t == null || pt == null)
                {
                    return;
                }

                Vector2 newAnchorsMin = new Vector2(t.anchorMin.x + t.offsetMin.x / pt.rect.width, t.anchorMin.y + t.offsetMin.y / pt.rect.height);
                Vector2 newAnchorsMax = new Vector2(t.anchorMax.x + t.offsetMax.x / pt.rect.width, t.anchorMax.y + t.offsetMax.y / pt.rect.height);
                t.anchorMin = newAnchorsMin;
                t.anchorMax = newAnchorsMax;
                t.offsetMin = t.offsetMax = new Vector2(0, 0);
            }
        }
#endif
    }
}