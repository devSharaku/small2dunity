using Events;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utils
{
    public class SceneLoader : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private LoadScene loadScene;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            AssignCallbacks();
        }

        private void OnDestroy()
        {
            UnassignCallbacks();
        }

        #endregion

        #region Private Methods

        private void AssignCallbacks()
        {
            loadScene.OnAction += LoadScene;
        }

        private void UnassignCallbacks()
        {
            loadScene.OnAction += LoadScene;
        }

        private void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        #endregion
    }
}