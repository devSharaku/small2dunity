using UnityEngine;

namespace Utils
{
    public interface IInputListener
    {
        #region Public Methods

        public void MovementPerformed(Vector2 movementVector);
        public void MoveTo(Vector2 position);

        #endregion
    }
}