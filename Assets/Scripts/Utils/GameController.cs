using UI.Enums;
using UI.Events;
using UnityEngine;

namespace Utils
{
    public class GameController : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private OpenPanel openPanel;

        #endregion

        #region Unity Callbacks

        private void Start()
        {
            openPanel.Invoke(PanelType.MainMenu);
        }

        #endregion
    }
}