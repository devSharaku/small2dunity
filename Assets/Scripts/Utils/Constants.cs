namespace Utils
{
    public static class Constants
    {
        #region Constants

        public static string MAIN_MENU_SCENE_NAME = "MainMenu";
        public static string GAMEPLAY_SCENE_NAME = "Game";

        #endregion
    }
}